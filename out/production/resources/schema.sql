DROP SCHEMA public CASCADE;

CREATE SEQUENCE seq1 START WITH 1;

CREATE TABLE customers (
       id INTEGER IDENTITY PRIMARY KEY NOT NULL,
       firstName VARCHAR(255) NULL,
       lastName VARCHAR(255) NULL,
       code VARCHAR(255) NULL
);


