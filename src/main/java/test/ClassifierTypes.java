package test;

import java.util.ArrayList;
import java.util.List;


public class ClassifierTypes {
    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    public void setCustomers(List<String> customers) {
        this.customers = customers;
    }

    private List<String> phones = new ArrayList();
    private List<String> customers = new ArrayList<>();

    public List<String> getPhones() {
        phones.add("phone_type.fixed");
        phones.add("phone_type.mobile");
        return phones;
    }

    public List<String> getCustomers() {
        customers.add("customer_type.private");
        customers.add("customer_type.corporate");
        return customers;
    }
}
