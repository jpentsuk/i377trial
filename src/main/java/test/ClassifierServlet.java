package test;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/api/classifiers")
public class ClassifierServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        ClassifierTypes classifierTypes = new ClassifierTypes();
        Classifiers classifiers = new Classifiers(classifierTypes.getCustomers(),classifierTypes.getPhones());
        new ObjectMapper().writeValue(resp.getOutputStream(),classifiers);

    }
}
