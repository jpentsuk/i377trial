package test;


import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/api/customers")
public class MainServlet extends HttpServlet{
        private static final long serialVersionUID = 1L;
        Dao dao = new Dao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //baasist läbi JSON i objektiks


        // peab contentType määrama, muidu testid ei läbi

        //schema.sql tabelis peavad firstname,lastname ja code olema NULL
        // kui on NOT NULL, siis osad testid ei läbi

        resp.setContentType("application/json");

        if(req.getParameter("id")==null)
        {
            new ObjectMapper().writeValue(resp.getOutputStream(), dao.getCustomers());
        }
        else
        {
            long id = Long.parseLong(req.getParameter("id"));
            new ObjectMapper().writeValue(resp.getOutputStream(), dao.getCustomer(id));
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/json");
        //objektist läbi JSONi kujult baasi
        Customer newcustomer = new ObjectMapper().readValue(req.getInputStream(),Customer.class);
        dao.insertCustomer(newcustomer);

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {


        if(req.getParameter("id")==null)
        {
            dao.deleteAll();
        }
        else
        {
            long id = Long.parseLong(req.getParameter("id"));
            dao.deleteOne(id);
        }
    }
}
