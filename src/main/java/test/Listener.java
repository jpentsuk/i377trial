package test;

import util.DataSourceProvider;
import util.DbUtil;
import util.FileUtil;
import util.PropertyLoader;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@WebListener
public class Listener implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent sce) {

        String statements = FileUtil.readFileFromClasspath("schema.sql");
        String dbUrl = PropertyLoader.getProperty("javax.persistence.jdbc.url");

        try (Connection c = DriverManager.getConnection(dbUrl)) {
            DbUtil.insertFromString(c, statements);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        System.out.println("Server is running on port 8080...");

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
