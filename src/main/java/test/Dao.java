package test;

import test.Customer;
import util.JpaUtil;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.util.List;

public class Dao {

    public void insertCustomer(Customer customer) {
        EntityManager em = null;

        try {
            em = JpaUtil.getFactory().createEntityManager();

            em.getTransaction().begin();

            em.persist(customer);

            em.getTransaction().commit();

        } finally {
            JpaUtil.closeQuietly(em);
        }
    }

    public Customer getCustomer(Long id) {
        EntityManager em = null;
        try {
            em = JpaUtil.getFactory().createEntityManager();
            TypedQuery<Customer> query = em.createQuery("SELECT e FROM Customer e WHERE e.id=:id",Customer.class);
            query.setParameter("id", id);
            Customer c = query.getSingleResult();
            return c;

        } finally {
            JpaUtil.closeQuietly(em);
        }
    }

    public List<Customer> getCustomers() {
        EntityManager em = null;

        try {
            em = JpaUtil.getFactory().createEntityManager();
            TypedQuery<Customer> query = em.createQuery("SELECT e FROM Customer e",Customer.class);
            List<Customer> customers = query.getResultList();
            return customers;

        } finally {
            JpaUtil.closeQuietly(em);
        }
    }

    public void deleteOne(Long id) {
        EntityManager em = null;
        try {
            em = JpaUtil.getFactory().createEntityManager();

            Customer customer = em.find(Customer.class, id);

            em.getTransaction().begin();
            em.remove(customer);
            em.getTransaction().commit();

        } finally {
            JpaUtil.closeQuietly(em);
        }
    }

    public void deleteAll() {
        EntityManager em = null;
        try {
            em = JpaUtil.getFactory().createEntityManager();
            //https://www.objectdb.com/java/jpa/query/jpql/delete

            Query query = em.createQuery("DELETE FROM Customer");
            Query query2 = em.createQuery("DELETE FROM Phone");

            em.getTransaction().begin();

            query2.executeUpdate();
            query.executeUpdate();

            em.getTransaction().commit();

        } finally {
            JpaUtil.closeQuietly(em);
        }
    }

}