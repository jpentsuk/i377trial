package test;

import java.util.ArrayList;
import java.util.List;

public class Classifiers {

    private List<String> customerTypes = new ArrayList<>();
    private List<String> phoneTypes = new ArrayList<>();

    public Classifiers(List<String> customerTypes, List<String> phoneTypes) {
        this.customerTypes = customerTypes;
        this.phoneTypes = phoneTypes;
    }

    public List<String> getCustomerTypes() {
        return customerTypes;
    }

    public void setCustomerTypes(List<String> customerTypes) {
        this.customerTypes = customerTypes;
    }

    public List<String> getPhoneTypes() {
        return phoneTypes;
    }

    public void setPhoneTypes(List<String> phoneTypes) {
        this.phoneTypes = phoneTypes;
    }

    @Override
    public String toString() {
        return "Classifiers{" +
                "customerTypes=" + customerTypes +
                ", phoneTypes=" + phoneTypes +
                '}';
    }
}
