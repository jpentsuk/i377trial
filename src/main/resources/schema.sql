DROP SCHEMA public CASCADE;

CREATE SEQUENCE seq1 START WITH 1;

CREATE TABLE CUSTOMER (
   id BIGINT NOT NULL PRIMARY KEY,
   first_name VARCHAR(255),
   last_name VARCHAR(255),
   code VARCHAR(255),
   type VARCHAR(255),
);

CREATE TABLE PHONE (
  id        BIGINT       NOT NULL PRIMARY KEY,
  value    VARCHAR(255) NOT NULL,
  type VARCHAR(255),
  customer_id BIGINT       NOT NULL,
  FOREIGN KEY (customer_id)
  REFERENCES CUSTOMER
    ON DELETE RESTRICT
);
